var index = (function indexFunction() {
    function getLetters() {
        $.busyLoadFull("show", {});

        var queryParameter = {
            url: $('#url').val(),
            tagType: $("#tagType option:selected").val(),
            bundle: $('#bundle').val()
        };
        return axios.get('/letters', {params: queryParameter})
            .then(result => {
                $('#quotient').html(result.data.quotient);
                $('#remainder').html(result.data.remainder);
            }).catch(error => {
                var message = error.response.data.message;
                if (error.response.data.errors) {
                    message = error.response.data.errors[0].defaultMessage;
                }
                alert(message);
            }).finally(() => {
                $.busyLoadFull("hide", {});
            });
    }

    return {
        getLetters
    }
}());