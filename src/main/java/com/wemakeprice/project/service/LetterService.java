package com.wemakeprice.project.service;

import com.wemakeprice.project.dto.LetterDto;
import com.wemakeprice.project.enums.TagType;
import com.wemakeprice.project.utils.Crawling;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class LetterService {
    private final Crawling crawling;

    public LetterDto.Response getLetters(LetterDto.Request letterRequestDto) throws Exception {
        /*
            crawlingToFile 은 deep하게 crawling하여 읽은 데이터가 많을 경우
            메모리가 아닌 파일로 저장하여 일정단위로 읽어 작업을 진행하려 했으나
            데이터 수집의 시간 소요, 외부정렬(External Sort)등 research와 개발의 시간적 제약이 발생하여
            메모리 처리범위로 가정하여 작업을 진행하였습니다.
         */
        //String html = this.crawlingToFile(letterRequestDto.getUrl());
        // 1. 모든 문자 입력 가능한 crawling data
        String html = this.crawling.crawling(letterRequestDto.getUrl());
        log.info("crawling: {}", html);

        // 2. 영어 숫자만 출력
        html = extractAlphabetAndNumber(html, letterRequestDto.getTagType());
        log.info("extractAlphabetAndNumber: {}", html);

        // 3. 영어/숫자 오름차순
        List<String> alphabetData = stringToArraySort(onlyAlphabet(html));
        List<String> numberData = stringToArraySort(onlyNumber(html));
        log.info("alphabetData: {}", alphabetData);
        log.info("numberData: {}", numberData);

        // 4. 교차출력
        StringBuilder mixString = mixAlphabetAndNumber(alphabetData, numberData);
        log.info("mixString: {}", mixString);

        // 5. 출력 묶음
        return packageLetter(mixString, letterRequestDto.getBundle());
    }

    private String extractAlphabetAndNumber(String html, TagType tagType) {
        String replaceHtml = html;
        if (TagType.EXCLUDE_HTML.equals(tagType)) {
            replaceHtml = replaceHtml.replaceAll("<(/)?([a-zA-Z]*)(\\s[a-zA-Z]*=[^>]*)?(\\s)*(/)?>", "");
        }
        return replaceHtml.replaceAll("[^0-9a-zA-Z]", "");
    }

    private String onlyAlphabet(String html) {
        return html.replaceAll("[^a-zA-Z]", "");
    }

    private String onlyNumber(String html) {
        return html.replaceAll("[^0-9]", "");
    }

    private List<String> stringToArraySort(String data) {
        List<String> arrays = Arrays.asList(data.split(""));

        Collections.sort(arrays, new Comparator<String>() {
            @Override
            public int compare(String s1, String s2) {
                int res = String.CASE_INSENSITIVE_ORDER.compare(s1, s2);
                if (res == 0) {
                    res = s1.compareTo(s2);
                }
                return res;
            }
        });
        return arrays;
    }

    private StringBuilder mixAlphabetAndNumber(List<String> alphabetData, List<String> numberData) {
        int alphabetDataSize = alphabetData.size();
        int numberDataSize = numberData.size();
        int maxSize = alphabetDataSize > numberDataSize ? alphabetDataSize : numberDataSize;

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < maxSize; i++) {
            if (i < alphabetDataSize) {
                sb.append(alphabetData.get(i));
            }

            if (i < numberDataSize) {
                sb.append(numberData.get(i));
            }
        }
        return sb;
    }

    private LetterDto.Response packageLetter(StringBuilder mixString, int bundle) {
        LetterDto.Response letterResponse = new LetterDto.Response();

        int total = new BigDecimal(mixString.length()).divide(new BigDecimal(bundle), BigDecimal.ROUND_DOWN).intValue();
        if (total == 0) {
            letterResponse.setRemainder(mixString.toString());
            return letterResponse;
        }

        int remainder = mixString.length() % bundle;
        if (remainder == 0) {
            total--;
        }

        int position = bundle;
        for (int i = 0; i < total; i++) {
            mixString.insert(position, ",");
            if (i < total - 1) {
                position += bundle + 1;
                continue;
            }
            if (remainder == 0) {
                letterResponse.setQuotient(mixString.toString());
            } else {
                letterResponse.setRemainder(mixString.substring(position + 1, mixString.length()));
                letterResponse.setQuotient(mixString.delete(position, mixString.length()).toString());
            }
        }
        return letterResponse;
    }
}
