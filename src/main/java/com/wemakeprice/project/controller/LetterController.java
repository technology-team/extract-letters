package com.wemakeprice.project.controller;

import com.wemakeprice.project.dto.LetterDto;
import com.wemakeprice.project.service.LetterService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/letters")
public class LetterController {
    private final LetterService letterService;

    @GetMapping()
    public LetterDto.Response getLetters(@Valid LetterDto.Request letterRequestDto) throws Exception {
        return letterService.getLetters(letterRequestDto);
    }
}
