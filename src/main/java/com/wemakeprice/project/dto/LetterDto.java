package com.wemakeprice.project.dto;

import com.wemakeprice.project.enums.TagType;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class LetterDto {

    @Setter
    @Getter
    public static class Request {
        @NotBlank
        @URL
        private String url;
        @NotNull
        private TagType tagType;
        @Min(value = 1, message = "bundle은 1보다 같거나 커야합니다.")
        private int bundle;
    }

    @Setter
    @Getter
    public static class Response {
        private String quotient;
        private String remainder;
    }
}
