package com.wemakeprice.project.enums;

public enum TagType {
    EXCLUDE_HTML(1),
    ALL(2);

    private int value;

    TagType(int value) {
        this.value = value;
    }
}
