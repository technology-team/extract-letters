package com.wemakeprice.project.utils;

import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtConfig;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.springframework.stereotype.Component;

@Component
public class Crawling {
    private FileStream fileStream = FileStream.getInstance();

    public String crawling(String url) throws Exception {
        Connection.Response response = Jsoup.connect(url)
                .method(Connection.Method.GET)
                .execute();
        return response.parse().html();
    }

    public String crawlingToFile(String url) throws Exception {
        CrawlConfig config = new CrawlConfig();
        config.setMaxDepthOfCrawling(0);
        config.setPolitenessDelay(500);
        config.setCrawlStorageFolder("data/crawl");

        PageFetcher pageFetcher = new PageFetcher(config);
        RobotstxtConfig robotstxtConfig = new RobotstxtConfig();
        RobotstxtServer robotstxtServer = new RobotstxtServer(robotstxtConfig, pageFetcher);

        int numberOfCrawlers = 1;
        CrawlController controller = new CrawlController(config, pageFetcher, robotstxtServer);
        controller.addSeed(url);
        controller.start(HtmlCrawler.class, numberOfCrawlers);

        return this.fileStream.fileInputStream();
    }
}
