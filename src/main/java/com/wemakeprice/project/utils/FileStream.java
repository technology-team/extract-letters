package com.wemakeprice.project.utils;

import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileStream {
    private static FileStream fileStream = new FileStream();

    public static FileStream getInstance() {
        return fileStream;
    }

    public void fileOutputStream(String data) {
        BufferedOutputStream bs = null;
        try {
            String filePath = this.getClass().getResource("").getPath() + "Output.txt";
            bs = new BufferedOutputStream(new FileOutputStream(filePath));
            bs.write(data.getBytes());
        } catch (Exception e) {
            e.getStackTrace();
            // TODO: handle exception
        } finally {
            try {
                bs.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public String fileInputStream() {
        FileInputStream fileStream = null;
        try {
            String filePath = this.getClass().getResource("").getPath() + "Output.txt";
            fileStream = new FileInputStream(filePath);

            byte[] readBuffer = new byte[fileStream.available()];
            while (fileStream.read(readBuffer) != -1) {
            }
            fileStream.close();
            return new String(readBuffer);

        } catch (Exception e) {
            e.getStackTrace();
            return new String();
        } finally {
            try {
                fileStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
