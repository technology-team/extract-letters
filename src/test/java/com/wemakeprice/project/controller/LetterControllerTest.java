package com.wemakeprice.project.controller;

import com.wemakeprice.project.dto.LetterDto;
import com.wemakeprice.project.service.LetterService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.validation.BindException;
import org.springframework.web.context.WebApplicationContext;

import static junit.framework.TestCase.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringRunner.class)
@WebMvcTest(value = LetterController.class)
public class LetterControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @MockBean
    private LetterService letterService;


    @Before
    public void initMockMvc() {
        mockMvc = webAppContextSetup(webApplicationContext)
                .build();
    }

    // getLetters Test(S)
    @Test
    public void getLetters_성공() throws Exception {
        // given
        LetterDto.Response letterDtoResponse = new LetterDto.Response();
        letterDtoResponse.setQuotient("A0a0,B0c1");
        letterDtoResponse.setRemainder("dd");
        given(this.letterService.getLetters(any())).willReturn(letterDtoResponse);

        // when & then
        mockMvc.perform(get("/letters?url={url}&tagType={tagType}&bundle={bundle}", "https://www.ics.uci.edu/", "ALL", 4))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.quotient").value(letterDtoResponse.getQuotient()))
                .andExpect(jsonPath("$.remainder").value(letterDtoResponse.getRemainder()));
    }

    @Test
    public void getLetters_url이유효하지않을때() throws Exception {
        // when & then
        mockMvc.perform(get("/letters?url={url}&tagType={tagType}&bundle={bundle}", "wrongUrl", "ALL", 4))
                .andDo(print())
                .andExpect(
                        (rslt) -> assertTrue(rslt.getResolvedException().getClass().isAssignableFrom(BindException.class))
                );
    }

    @Test
    public void getLetters_url이_null일경우() throws Exception {
        // when & then
        mockMvc.perform(get("/letters?url={url}&tagType={tagType}&bundle={bundle}", null, "ALL", 4))
                .andDo(print())
                .andExpect(
                        (rslt) -> assertTrue(rslt.getResolvedException().getClass().isAssignableFrom(BindException.class))
                );
    }

    @Test
    public void getLetters_url이_공백일경우() throws Exception {
        // when & then
        mockMvc.perform(get("/letters?url={url}&tagType={tagType}&bundle={bundle}", " ", "ALL", 4))
                .andDo(print())
                .andExpect(
                        (rslt) -> assertTrue(rslt.getResolvedException().getClass().isAssignableFrom(BindException.class))
                );
    }

    @Test
    public void getLetters_tagType이_잘못된경우() throws Exception {
        // when & then
        mockMvc.perform(get("/letters?url={url}&tagType={tagType}&bundle={bundle}", "https://www.ics.uci.edu/", "WRONG", 4))
                .andDo(print())
                .andExpect(
                        (rslt) -> assertTrue(rslt.getResolvedException().getClass().isAssignableFrom(BindException.class))
                );
    }

    @Test
    public void getLetters_tagType이_null인경우() throws Exception {
        // when & then
        mockMvc.perform(get("/letters?url={url}&tagType={tagType}&bundle={bundle}", "https://www.ics.uci.edu/", null, 4))
                .andDo(print())
                .andExpect(
                        (rslt) -> assertTrue(rslt.getResolvedException().getClass().isAssignableFrom(BindException.class))
                );
    }

    @Test
    public void getLetters_tagType이_공백인경우() throws Exception {
        // when & then
        mockMvc.perform(get("/letters?url={url}&tagType={tagType}&bundle={bundle}", "https://www.ics.uci.edu/", " ", 4))
                .andDo(print())
                .andExpect(
                        (rslt) -> assertTrue(rslt.getResolvedException().getClass().isAssignableFrom(BindException.class))
                );
    }

    @Test
    public void getLetters_bundle이_0인경우() throws Exception {
        // when & then
        mockMvc.perform(get("/letters?url={url}&tagType={tagType}&bundle={bundle}", "https://www.ics.uci.edu/", "ALL", 0))
                .andDo(print())
                .andExpect(
                        (rslt) -> assertTrue(rslt.getResolvedException().getClass().isAssignableFrom(BindException.class))
                );
    }

    @Test
    public void getLetters_bundle이_음수인경우() throws Exception {
        // when & then
        mockMvc.perform(get("/letters?url={url}&tagType={tagType}&bundle={bundle}", "https://www.ics.uci.edu/", "ALL", -5))
                .andDo(print())
                .andExpect(
                        (rslt) -> assertTrue(rslt.getResolvedException().getClass().isAssignableFrom(BindException.class))
                );
    }

    @Test
    public void getLetters_bundle이_null인경우() throws Exception {
        // when & then
        mockMvc.perform(get("/letters?url={url}&tagType={tagType}&bundle={bundle}", "https://www.ics.uci.edu/", "ALL", null))
                .andDo(print())
                .andExpect(
                        (rslt) -> assertTrue(rslt.getResolvedException().getClass().isAssignableFrom(BindException.class))
                );
    }

    @Test
    public void getLetters_bundle이_문자형태의_숫자인경우() throws Exception {
        // given
        LetterDto.Response letterDtoResponse = new LetterDto.Response();
        letterDtoResponse.setQuotient("A0a0,B0c1");
        letterDtoResponse.setRemainder("dd");
        given(this.letterService.getLetters(any())).willReturn(letterDtoResponse);

        // when & then
        mockMvc.perform(get("/letters?url={url}&tagType={tagType}&bundle={bundle}", "https://www.ics.uci.edu/", "ALL", "4"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.quotient").value(letterDtoResponse.getQuotient()))
                .andExpect(jsonPath("$.remainder").value(letterDtoResponse.getRemainder()));
    }

    @Test
    public void getLetters_bundle이_문자형태인경우() throws Exception {
        // when & then
        mockMvc.perform(get("/letters?url={url}&tagType={tagType}&bundle={bundle}", "https://www.ics.uci.edu/", "ALL", "four"))
                .andDo(print())
                .andExpect(
                        (rslt) -> assertTrue(rslt.getResolvedException().getClass().isAssignableFrom(BindException.class))
                );
    }
    // getLetters Test(E)
}