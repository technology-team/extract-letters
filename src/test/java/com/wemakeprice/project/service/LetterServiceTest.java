package com.wemakeprice.project.service;

import com.wemakeprice.project.dto.LetterDto;
import com.wemakeprice.project.enums.TagType;
import com.wemakeprice.project.utils.Crawling;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class LetterServiceTest {
    private LetterService letterService;

    @Mock
    private Crawling crawling;

    @Before
    public void init() {
        this.letterService = new LetterService(this.crawling);
    }

    @Test
    public void getLetters_결과가영문_숫자조합() throws Exception {
        String html = "<body>@#$DataA54한글%&中ナムみんなで育asjkdhasduwer1232385444asasللغة العربية</body>";
        when(this.crawling.crawling(anyString())).thenReturn(html);

        LetterDto.Request letterRequestDto = new LetterDto.Request();
        letterRequestDto.setUrl("http://site.com");
        letterRequestDto.setTagType(TagType.EXCLUDE_HTML);
        letterRequestDto.setBundle(4);
        LetterDto.Response letterResponseDto = this.letterService.getLetters(letterRequestDto);

        assertThat(letterResponseDto.getQuotient()).isEqualTo("A1a2,a2a3,a3a4,a4D4,d4d5,e5h8,jkrs,ssst");
        assertThat(letterResponseDto.getRemainder()).isEqualTo("uw");
    }

    @Test
    public void getLetters_결과가영어만존재() throws Exception {
        String html = "<body>@#$DataA한글%&中ナムみんなで育للغة العربية</body>";
        when(this.crawling.crawling(anyString())).thenReturn(html);

        LetterDto.Request letterRequestDto = new LetterDto.Request();
        letterRequestDto.setUrl("http://site.com");
        letterRequestDto.setTagType(TagType.EXCLUDE_HTML);
        letterRequestDto.setBundle(4);
        LetterDto.Response letterResponseDto = this.letterService.getLetters(letterRequestDto);

        assertThat(letterResponseDto.getQuotient()).isEqualTo("AaaD");
        assertThat(letterResponseDto.getRemainder()).isEqualTo("t");
    }

    @Test
    public void getLetters_결과가숫자만존재() throws Exception {
        String html = "<body>@#$865224991387341541한글%&中ナムみんなで育للغة العربية</body>";
        when(this.crawling.crawling(anyString())).thenReturn(html);

        LetterDto.Request letterRequestDto = new LetterDto.Request();
        letterRequestDto.setUrl("http://site.com");
        letterRequestDto.setTagType(TagType.EXCLUDE_HTML);
        letterRequestDto.setBundle(4);
        LetterDto.Response letterResponseDto = this.letterService.getLetters(letterRequestDto);

        assertThat(letterResponseDto.getQuotient()).isEqualTo("1112,2334,4455,6788");
        assertThat(letterResponseDto.getRemainder()).isEqualTo("99");
    }

    @Test(expected = ArithmeticException.class)
    public void getLetters_bundle이_0인경우() throws Exception {
        String html = "<body>@#$DataA한글%&中ナムみんなで育للغة العربية</body>";
        when(this.crawling.crawling(anyString())).thenReturn(html);

        LetterDto.Request letterRequestDto = new LetterDto.Request();
        letterRequestDto.setUrl("http://site.com");
        letterRequestDto.setTagType(TagType.EXCLUDE_HTML);
        letterRequestDto.setBundle(0);
        LetterDto.Response letterResponseDto = this.letterService.getLetters(letterRequestDto);

        assertThat(letterResponseDto.getQuotient()).isEqualTo("1112,2334,4455,6788");
        assertThat(letterResponseDto.getRemainder()).isEqualTo("99");
    }

    @Test
    public void getLetters_bundle이_html길이보다_작은경우() throws Exception {
        String html = "<body>@#$DataA54한글%&中ナムみんなで育asjkdhasduwer1232385444asasللغة العربية</body>";
        when(this.crawling.crawling(anyString())).thenReturn(html);

        LetterDto.Request letterRequestDto = new LetterDto.Request();
        letterRequestDto.setUrl("http://site.com");
        letterRequestDto.setTagType(TagType.ALL);
        letterRequestDto.setBundle(5);
        LetterDto.Response letterResponseDto = this.letterService.getLetters(letterRequestDto);

        assertThat(letterResponseDto.getQuotient()).isEqualTo("A1a2a,2a3a3,a4a4b,4b4D5,d5d8d,dehjk,oorss,sstuw");
        assertThat(letterResponseDto.getRemainder()).isEqualTo("yy");
    }

    @Test
    public void getLetters_bundle이_html길이보다_큰경우() throws Exception {
        String html = "<body>@#$DataA54한글%&中ナムみんなで育asjkdhasduwer1232385444asasللغة العربية</body>";
        when(this.crawling.crawling(anyString())).thenReturn(html);

        LetterDto.Request letterRequestDto = new LetterDto.Request();
        letterRequestDto.setUrl("http://site.com");
        letterRequestDto.setTagType(TagType.ALL);
        letterRequestDto.setBundle(100);
        LetterDto.Response letterResponseDto = this.letterService.getLetters(letterRequestDto);

        assertThat(letterResponseDto.getQuotient()).isNull();
        assertThat(letterResponseDto.getRemainder()).isEqualTo("A1a2a2a3a3a4a4b4b4D5d5d8ddehjkoorsssstuwyy");
    }

    @Test
    public void getLetters_출력묶음이남지않을경우() throws Exception {
        String html = "<body>@#$DataA54한글%&中ナムみんなで育asjkdhasduwer1232385444asasللغة العربية</body>";
        when(this.crawling.crawling(anyString())).thenReturn(html);

        LetterDto.Request letterRequestDto = new LetterDto.Request();
        letterRequestDto.setUrl("http://site.com");
        letterRequestDto.setTagType(TagType.ALL);
        letterRequestDto.setBundle(2);
        LetterDto.Response letterResponseDto = this.letterService.getLetters(letterRequestDto);

        assertThat(letterResponseDto.getQuotient()).isEqualTo("A1,a2,a2,a3,a3,a4,a4,b4,b4,D5,d5,d8,dd,eh,jk,oo,rs,ss,st,uw,yy");
        assertThat(letterResponseDto.getRemainder()).isNull();
    }
}