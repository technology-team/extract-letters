package com.wemakeprice.project.utils;

import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@Slf4j
public class CrawlingTest {

    private Crawling crawling;

    @Before
    public void init() {
        this.crawling = new Crawling();
    }

    @Test
    public void crawling_영문사이트() throws Exception {
        String url = "https://www.ics.uci.edu/";
        String html = this.crawling.crawling(url);
        log.info("crawling: {}", html);
        assertThat(html).isNotEmpty();
    }

    @Test
    public void crawling_아랍어사이트() throws Exception {
        String url = "https://ar.yna.co.kr/";
        String html = this.crawling.crawling(url);
        log.info("crawling: {}", html);
        assertThat(html).isNotEmpty();
    }

    @Test
    public void crawling_한국어사이트() throws Exception {
        String url = "https://www.naver.com/";
        String html = this.crawling.crawling(url);
        log.info("crawling: {}", html);
        assertThat(html).isNotEmpty();
    }

    @Test
    public void crawling_일본사이트() throws Exception {
        String url = "https://dream.jp/";
        String html = this.crawling.crawling(url);
        log.info("crawling: {}", html);
        assertThat(html).isNotEmpty();
    }

    @Test
    public void crawling_중국사이트() throws Exception {
        String url = "https://www.baidu.com/";
        String html = this.crawling.crawling(url);
        log.info("crawling: {}", html);
        assertThat(html).isNotEmpty();
    }
}